require 'sinatra'
require 'sinatra/streaming'
class StreamAPI < Sinatra::Base
  helpers Sinatra::Streaming
  get '/stream' do
    puts "STARTING"
    stream do |out|
      loop do
        puts "PING"
        out.puts "Hello!"
        out.flush
        sleep 1
      end
    end
  end
  run! if app_file == $0
end
